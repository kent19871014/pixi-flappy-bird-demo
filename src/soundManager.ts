import { ResourcesList } from "./resources";
import { Howl, Howler } from 'howler';


class SoundInfo {
    public soundID: string;
    public path: string;
    public sound: Howl;
    constructor(_id: string, url: string) {
        this.soundID = _id;
        this.path = url;
        this.load();
    }
    public load() {
        this.sound = new Howl({ src: this.path });
    }
}

export class SoundManager {
    private static soundList: Array<SoundInfo> = new Array<SoundInfo>();
    public static load() {
        ResourcesList.sound.forEach(item => {
            const info = new SoundInfo(item.id, item.path);

            this.soundList.push(info);
        });
    }
    public static play(id, loop = false) {
        this.soundList.forEach(item => {
            if (item.soundID === id) {
                item.sound.loop(loop);
                item.sound.play();
            }
        });
    }
}
