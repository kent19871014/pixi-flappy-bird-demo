const webpack = require('webpack');
const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const sourcePath = path.join(__dirname, './src');
const buildPath = path.join(__dirname, './dist');
const assetsPath = path.join(__dirname, './src/assets');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: "./src/main.ts",
    output: {
        path: buildPath,
        publicPath: "",
        filename: "app-[hash].js",
    },
    // Enable sourcemaps for debugging webpack's output.
    devtool: "inline-source-map",
    resolve: {
        // Add '.ts' as resolvable extensions.
        extensions: [".webpack-loader.js", ".web-loader.js", ".loader.js", ".ts", ".js"],
        modules: [
            path.resolve(__dirname, 'node_modules'),
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(sourcePath, 'index.html'),
            path: buildPath,
            filename: 'index.html',
        }),
        new CopyWebpackPlugin([{
            from: path.resolve(__dirname, './src/assets'),
            to: "assets",
            ignore: ['.*']
        }])
    ],
    module: {
        rules: [{
            test: /\.ts$/,
            use: ["ts-loader"]
        }, {
            test: /\.(png|gif|jpg|svg)$/,
            include: assetsPath,
            use: 'url-loader?limit=20480&name=assets/[name]-[hash].[ext]',
        }, {
            test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
            loader: 'url-loader',
            options: {
                limit: 10000,
                name: "url-loader?limit=20480&name=assets/audio/[name]-[hash].[ext]"
            }
        }, ]
    },
    devServer: {
        contentBase: sourcePath,
        historyApiFallback: true,
        port: 8080,
        compress: false,
        inline: true,
        hot: true,
        host: '0.0.0.0',
        disableHostCheck: true,
        stats: {
            assets: true,
            children: false,
            chunks: false,
            hash: false,
            modules: false,
            publicPath: false,
            timings: true,
            version: false,
            warnings: true,
            colors: {
                green: '\u001b[32m',
            },
        },
    },
    // Omit "externals" if you don't have any. Just an example because it's
    // common to have them.
    externals: {
        // Don't bundle giant dependencies, instead assume they're available in
        // the html doc as global variables node module name -> JS global
        // through which it is available
        //"pixi.js": "PIXI"
    }
};