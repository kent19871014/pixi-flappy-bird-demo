import * as PIXI from "pixi.js";
import { SoundManager } from './soundManager';
import { Game, GAME_STATE } from "./main";

const GRAVITY = 5;
const MAX_ROT = Math.PI * 0.5;
export class Player {
    private _player: PIXI.AnimatedSprite;
    private velocity: number = 0;
    private flapVelocity: number = 20;
    private bound: { x: number, y: number } = { x: 0, y: 0 };

    constructor() {
        const { stage } = Game.app;
        const { spritesheet } = Game.resources.bird
        const frames = Object.keys(spritesheet.textures).map(item => spritesheet.textures[item]);

        this._player = new PIXI.AnimatedSprite(frames);
        this._player.anchor.set(0.5);
        this._player.animationSpeed = .2;
        this._player.play();
        this.reset();
        stage.addChild(this._player);
    }

    get player(): PIXI.AnimatedSprite {
        return this._player;
    }

    public setBound({ x, y }): void {
        this.bound = {
            x,
            y: y - this._player.width / 2
        };
    }

    public updatePos(): void {
        this.velocity -= GRAVITY * 0.1;
        this._player.y -= this.velocity * 0.1;

        // check bound
        if (this._player.y >= this.bound.y) {
            this._player.y = this.bound.y;
            this._player.stop();
        }

        // check rotation
        // this._player.rotation += playerVelRot;
        let h = this.velocity >= 0.75 ? 0.75 : Math.abs(this.velocity) <= 2 ? 2 : Math.abs(this.velocity)

        this._player.rotation = (h -= 1)

        if (this._player.rotation >= MAX_ROT) {
            this._player.rotation = MAX_ROT;
        }
    }

    public fly(): void {
        this.velocity = this.flapVelocity;
        SoundManager.play("wing");
    }

    public reset(): void {
        const { screen } = Game.app;
        this._player.x = screen.width / 3;
        this._player.y = screen.height / 2.2;
        this._player.rotation = 0;
        this._player.play();
    }
}