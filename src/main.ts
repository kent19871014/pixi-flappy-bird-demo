import * as PIXI from "pixi.js";
import { ResourcesList } from "./resources";
import { SoundManager } from './soundManager';
import { Player } from "./player";
import { Ground } from "./ground";
import { Pipe } from "./pipe";
import { Score } from "./score";

export enum GAME_STATE {
    READY,
    PLAYING,
    DEAD,
}

export class Game {
    private failedFiles: Array<string> = [];
    private completedFiles: Array<string> = [];
    private bird: Player;
    private ground: Ground;
    private pipeList: Array<Pipe> = [];
    private readyUI: PIXI.Sprite;
    private gameOverUI: PIXI.Sprite;
    private score: Score;
    private okBtn: PIXI.Sprite;

    public static app: PIXI.Application;
    public static resources: any;
    public static gameState: GAME_STATE = GAME_STATE.READY;



    constructor() {
        const canvas = document.getElementById("game-app") as HTMLCanvasElement;
        Game.app = new PIXI.Application({
            width: 288,
            height: 512,
            backgroundColor: 0x1099bb,
            resolution: window.devicePixelRatio || 1,
            view: canvas
        });

        window.onresize = this.resize;
        this.resize();
        this.loadAssets();
    }

    private loadAssets(): void {
        const { screen, stage, ticker } = Game.app;

        const assetsLoader = new PIXI.Loader();
        SoundManager.load();
        ResourcesList.img.forEach(element => {
            assetsLoader.add(element.id, element.path);
        });

        assetsLoader.load((loader, resources) => {
            Game.resources = resources;
        });

        //可取得下載進度
        assetsLoader.onProgress.add((event: any) => {
            console.log("onProgress: ", event);
        });

        //載入檔案錯誤時
        assetsLoader.onError.add((target: any, event: any, error: any) => {
            this.failedFiles.push(error.name);
            console.log("onError: ", error);
        });

        //每個檔案載入時都會呼叫
        assetsLoader.onLoad.add((event: any, target: any) => {
            this.completedFiles.push(target.name);
            console.log("onLoad: ", target);
        });

        //全部下載完成後
        assetsLoader.onComplete.add(() => {
            if (this.failedFiles.length == 0) {
                console.log("all file completed", Game.resources);


                const bkg = new PIXI.Sprite(Game.resources.background.texture);
                stage.addChild(bkg);

                const startX = 0;

                this.pipeList.push(new Pipe(startX));
                this.pipeList.push(new Pipe(startX + 200));
                this.score = new Score();
                this.ground = new Ground();
                this.bird = new Player();
                this.bird.setBound({
                    x: screen.x,
                    y: screen.height - this.ground.getSize().height,
                })

                this.readyUI = new PIXI.Sprite(Game.resources.getReady.texture);
                this.readyUI.anchor.set(0.5);
                this.readyUI.x = screen.width / 2;
                this.readyUI.y = screen.height / 3;
                stage.addChild(this.readyUI);

                this.gameOverUI = new PIXI.Sprite(Game.resources.gameOver.texture);
                this.gameOverUI.anchor.set(0.5);
                this.gameOverUI.x = screen.width / 2;
                this.gameOverUI.y = screen.height / 3;
                this.gameOverUI.visible = false;
                stage.addChild(this.gameOverUI);

                this.okBtn = new PIXI.Sprite(Game.resources.ok.texture);
                this.okBtn.anchor.set(0.5);
                this.okBtn.x = screen.width / 2;
                this.okBtn.y = screen.height / 2;
                this.okBtn.visible = false;
                this.okBtn.interactive = true;
                this.okBtn.on("pointerdown", () => {
                    this.init();
                });
                stage.addChild(this.okBtn);

                stage.interactive = true;
                stage.on("pointerdown", () => {
                    switch (Game.gameState) {
                        case GAME_STATE.READY:
                            this.readyUI.visible = false;
                            Game.gameState = GAME_STATE.PLAYING;
                        case GAME_STATE.PLAYING:
                            this.bird.fly();
                            break;
                    }
                })

                ticker.add(this.gameTimer.bind(this));

            }
            else {
                console.log("Loading...failed: could not load " + this.failedFiles);
            }
        });
    }

    private gameTimer(delta): void {
        const speed = delta * 1.5;
        const { x, y, width, height } = this.bird.player;

        switch (Game.gameState) {
            case GAME_STATE.READY:
                this.ground.updatePos(speed);
                break;
            case GAME_STATE.PLAYING:
                this.bird.updatePos();
                this.ground.updatePos(speed);
                this.pipeList.map((item) => {
                    item.updatePos(speed);
                    if (!item.isPass) {
                        if (item.checkCollision(x - width / 2, y - height / 2, width, height)) {
                            Game.gameState = GAME_STATE.DEAD;
                            SoundManager.play("hit");
                            SoundManager.play("die");
                            this.gameOver();
                        }

                        if (!item.isGetPoint && item.checkGetPoint(x - width / 2, y - height / 2, width, height)) {
                            this.score.add();
                        }

                        item.checkThrough(x - width / 2, y - height / 2, width, height)
                    }
                })
                if (this.ground.checkCollision(x - width / 2, y - height / 2, width, height)) {
                    Game.gameState = GAME_STATE.DEAD;
                    SoundManager.play("hit");
                    this.gameOver();
                }
                break;
            case GAME_STATE.DEAD:
                this.bird.updatePos();
                break;
            default:
                break;
        }


    }

    private gameOver(): void {
        this.gameOverUI.visible = true;
        this.okBtn.visible = true;
        Game.app.stage.interactive = false;
    }

    private init(): void {
        this.gameOverUI.visible = false;
        this.okBtn.visible = false;
        this.bird.reset();
        this.pipeList.map((item, i) => {
            item.reset(i * 200);
        });
        this.readyUI.visible = true;
        this.score.reset();
        Game.gameState = GAME_STATE.READY;
        Game.app.stage.interactive = true;
        SoundManager.play("swoosh");
    }

    private resize(): void {
        if (window.innerWidth / window.innerHeight <= window.devicePixelRatio) {
            if(window.innerHeight * (9 / 16) > window.innerWidth ) {
                Game.app.view.style.height = window.innerWidth * (16 / 9) + "px";
            }
            else {
                Game.app.view.style.height = window.innerHeight + 'px';
            }
        } 
        else {
            Game.app.view.style.height = window.innerHeight + 'px';
        }
    }
}

new Game();
