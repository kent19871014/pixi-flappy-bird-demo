class Resources {
    public id: string;
    public path: string;

    constructor(id: string, path: string) {
        this.id = id;
        this.path = path;
    }
}

export class ResourcesList {
    public static img = [
        new Resources("bird", "assets/redbird.json"),
        new Resources("score", "assets/number.json"),
        new Resources("getReady", "assets/message.png"),
        new Resources("background", "assets/background-day.png"),
        new Resources("ground", "assets/base.png"),
        new Resources("pipe", "assets/pipe-green.png"),
        new Resources("gameOver", "assets/gameover.png"),
        new Resources("ok", "assets/ok.jpg"),
    ];

    public static sound = [
        new Resources("die", "assets/audio/die.wav"),
        new Resources("hit", "assets/audio/hit.wav"),
        new Resources("point", "assets/audio/point.wav"),
        new Resources("swoosh", "assets/audio/swoosh.wav"),
        new Resources("wing", "assets/audio/wing.wav"),
    ];
}