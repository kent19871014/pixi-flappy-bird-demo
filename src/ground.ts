import * as PIXI from "pixi.js";
import { Game } from "./main";

export class Ground {
    private _ground: PIXI.Sprite;
    private endX: number = 0;
    private bound: { x: number, y: number } = { x: 0, y: 0 };

    constructor() {
        const { screen, stage } = Game.app;
        this._ground = new PIXI.Sprite(Game.resources.ground.texture);
        this._ground.x = 0;
        this._ground.y = screen.height - this._ground.height;

        stage.addChild(this._ground);

        this.endX = -Math.abs(screen.width - this._ground.width);
    }

    get node(): PIXI.Sprite {
        return this._ground;
    }

    public getSize(): {width: number, height: number} {
        return {
            width: this._ground.width,
            height: this._ground.height,
        }
    }

    public updatePos(delta: number): void {
        this._ground.x -= delta;

        if (this._ground.x < this.endX) {
            this._ground.x = 0;
        }
    }

    public checkCollision(x: number, y: number, width: number, height: number) {
        if (!((y + width) <= this._ground.y)) {
            return true;
        }

        return false;
    }
}