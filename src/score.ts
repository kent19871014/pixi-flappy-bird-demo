import * as PIXI from "pixi.js";
import { Game } from "./main";
import { SoundManager } from "./soundManager";

const NUM_WIDTH = 24;

export class Score {
    private spriteList: Array<PIXI.Sprite> = [];
    private scoreContainer: PIXI.Container;
    private centerPos: { x: number, y: number };
    private score: number = 0;

    constructor() {
        const { screen, stage } = Game.app;
        this.centerPos = {
            x: screen.width / 2,
            y: screen.height / 5
        }

        this.scoreContainer = new PIXI.Container;
        this.scoreContainer.y = this.centerPos.y;

        stage.addChild(this.scoreContainer);
        this.addNumberDigital();
    }

    private addNumberDigital(): void {
        const num = new PIXI.Sprite(Game.resources.score.textures.num_0);

        num.anchor.set(0.5);
        num.scale.set(0.5);
        this.scoreContainer.addChild(num);
        this.spriteList.push(num);
        this.resetPos();
    }

    private resetPos(): void {
        for (let i = this.spriteList.length - 1; i >= 0; i--) {
            const item = this.scoreContainer.getChildAt(i) as PIXI.Sprite;
            if (i + 1 < this.spriteList.length) {
                const next = this.scoreContainer.getChildAt(i + 1) as PIXI.Sprite;
                item.x = NUM_WIDTH * 0.5 + next.x;
            }
        }
        this.scoreContainer.pivot.x = this.scoreContainer.width / 2;
        this.scoreContainer.x = this.centerPos.x;
    }

    private setNumber() {
        const numStr = this.score.toString().split("").reverse();
        const len = numStr.length;

        for (let i = 0; i < len; i++) {
            if (!this.spriteList[i]) {
                this.addNumberDigital();
            }
            this.spriteList[i].texture = Game.resources.score.textures["num_" + numStr[i]];
        }

        SoundManager.play("point");
    }

    public add(): void {
        this.score += 1;
        this.setNumber();
    }

    public reset(): void {
        this.score = 0;
        for (let i = 0; i < this.spriteList.length; i++) {
            const item = this.spriteList[i];
            item.destroy();
        }
        this.spriteList = [];
        this.addNumberDigital();
    }
}