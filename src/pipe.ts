import * as PIXI from "pixi.js";
import { Game } from "./main";

export class Pipe {
    private _pipeUp: PIXI.Sprite;
    private _pipeDown: PIXI.Sprite;
    private readonly base = Game.app.screen.height * 0.79;;
    private readonly pipeGapSize = 100;
    private _isPass: boolean = false;
    private _isGetPoint: boolean = false;

    constructor(startX: number) {
        const { stage } = Game.app;

        this._pipeUp = new PIXI.Sprite(Game.resources.pipe.texture);
        this._pipeDown = new PIXI.Sprite(Game.resources.pipe.texture);
        this._pipeUp.pivot.y = this._pipeUp.height;
        this._pipeUp.scale.x = -1;
        this._pipeUp.rotation = Math.PI * 1;
        stage.addChild(this._pipeUp);
        stage.addChild(this._pipeDown);
        this.reset(startX);
    }

    public reset(x: number) {
        const { screen, stage } = Game.app;

        let randomGap = Math.random() * (this.base * 0.6 - this.pipeGapSize);

        this._isPass = false;
        this._isGetPoint = false;
        randomGap += this.base * 0.2;

        this._pipeUp.x = screen.width + this._pipeUp.width + x;
        this._pipeDown.x = screen.width + this._pipeDown.width + x;

        this._pipeUp.y = randomGap + - this._pipeUp.height;
        this._pipeDown.y = randomGap + this.pipeGapSize;
    }

    public updatePos(delta) {
        this._pipeUp.x -= delta;
        this._pipeDown.x -= delta;

        if (this._pipeUp.x < -this._pipeUp.width) {
            this.reset(0);
        }
    }

    public checkCollision(x: number, y: number, width: number, height: number) {
        if (!(x + width < this._pipeUp.x || this._pipeUp.x + this._pipeUp.width < x || y > this._pipeUp.y + this._pipeUp.height)) {
            return true;
        }

        if (!(x + width < this._pipeDown.x || this._pipeDown.x + this._pipeDown.width < x || y + height < this._pipeDown.y)) {
            return true;
        }

        return false;
    }

    public checkThrough(x: number, y: number, width: number, height: number): Boolean {
        if (x > this._pipeUp.x + this._pipeUp.width) {
            this._isPass = true;
            return true;
        }
        return false;
    }

    public checkGetPoint(x: number, y: number, width: number, height: number): Boolean {
        if (x + width / 2 > this._pipeUp.x + this._pipeUp.width / 2) {
            this._isGetPoint = true;
            return true;
        }
        return false;
    }

    get isPass(): Boolean {
        return this._isPass;
    }

    get isGetPoint(): Boolean {
        return this._isGetPoint;
    }
}